<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    // return view('admin.index');
    return;

});

Route::controllers([
  'auth' => 'Auth\AuthController',
  'password' => 'Auth\PasswordController',
]);



Route::group([
  'middleware' => ['auth'],
  'prefix' => 'dashboard'
  ], function () {

        Route::get('/', 'DashboardController@index');

        Route::delete('users/deleteImage/{id}','UserController@deleteImage');
        Route::post('users/getUserDetials','UserController@getUserDetials');
        Route::resource('users', 'UserController', ['except' => ['show']]);

        Route::delete('categories/deleteImage/{id}','CategoryController@deleteImage');
        Route::resource('categories', 'CategoryController', ['except' => ['show']]);   

        Route::delete('brands/deleteImage/{id}','BrandController@deleteImage');
        Route::resource('brands', 'BrandController', ['except' => ['show']]);

        Route::delete('ads/deleteImage/{id}','AdsController@deleteImage');
        Route::resource('ads', 'AdsController', ['except' => ['show']]);

        Route::delete('about/deleteImage/{id}','AboutController@deleteImage');
        Route::resource('about', 'AboutController', ['only' => ['index','update']]);

        Route::resource('settings', 'SettingController', ['only' => ['index','update']]);
});

Route::resource('comments', 'CommentController', ['only' => ['store','destroy']]);