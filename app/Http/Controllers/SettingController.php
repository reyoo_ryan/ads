<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use App\Http\Requests;

class SettingController extends Controller
{
    public function index()
    {
      	$setting = Setting::getAll();
      	$active = 'settings';
        return view('admin.settings.edit',compact('setting','active'));
    }

    public function update(Requests\EditSettingRequest $request)
    {
        $setting = $request->except(['_token','_method']);          

        if (Setting::updateSetting($setting)) {
                
             $request->session()->flash('status','success');
             $request->session()->flash('message','تم تعديل البيانات بنجاح');             

        }else{
             $request->session()->flash('status','danger');
             $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
        }

        return redirect('/dashboard/settings');


    }
}
