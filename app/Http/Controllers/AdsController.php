<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ads;
use App\Models\Category;
use App\Models\Brand;
use App\Models\User;
use App\Models\AdsImgs;
use App\Http\Requests;

class AdsController extends Controller
{
    public function index()
    {
      	$ads = Ads::all();
      	$active = 'ads';
        return view('admin.ads.index',compact('ads','active'));
    }

    public function create()
    {
      	$active = 'add_ads';
      	$categories = Category::all();
      	$users = User::getUsersSelectForm();

        return view('admin.ads.create',compact('active','categories','users'));
    }

    public function store(Requests\CreateAdsRequest $request)
    {

      if(count($request->file('imgs')) > 4){

            $request->session()->flash('custErrors', ['imgs[]' => 'فقط 4 صور على الاكثر']);
            return redirect()
                    ->back()
                    ->withInput();

      }
      
        $ad = $request->except(['brands','imgs']);

        $brands = $request->get('brands');
        
  	    if ($createdAds = Ads::createAds($ad)) {
  	        
            $createdAds->brands()->attach($brands);

  	        $request->session()->flash('status','success');
  	        $request->session()->flash('message','تم إضافة الإعلان بنجاح');             


  	    }else{
  	        
  	        $request->session()->flash('status','danger');
  	        $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
  	    }

  	    return redirect('/dashboard/ads');

    }

     public function edit($id , Request $request)
    {
        $active = 'ads';
        $categories = Category::all();
        $users = User::getUsersSelectForm();

        if($ad = Ads::find($id)){
            
            return view('admin.ads.edit', compact('active', 'ad','categories','users'));

        }else{

             $request->session()->flash('status','danger');
             $request->session()->flash('message','هذا الإعلان غير موجود');
             return redirect('/dashboard/ads');
        }
    }

 
    public function update(Requests\EditAdsRequest $request, $id)
    {

        $images = Ads::find($id)->imgs->count();

        $requestImagesCount = count($request->file('imgs'));
        
        if (is_null($request->file('imgs')[0])) {
            $requestImagesCount = 0;
        }

        if(($requestImagesCount + $images ) > 4){

            $request->session()->flash('custErrors', ['imgs[]' => 'فقط 4 صور على الاكثر']);
            return redirect()
                    ->back()
                    ->withInput();
        
        }

        $brands = $request->get('brands');
        $ads = $request->except(['imgs','brands']);

        if (Ads::updateAds($ads,$id)) {

             Ads::find($id)->brands()->sync($brands);

             $request->session()->flash('status','success');
             $request->session()->flash('message','تم تعديل البيانات بنجاح');             

        }else{
             $request->session()->flash('status','danger');
             $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
        }

        return redirect('/dashboard/ads/'.$id.'/edit');


    }


    public function destroy(Request $request, $id)
    {
        $active = 'ads';

        if($ads = Ads::find($id)){

            try {

                $ads->delete();
                $request->session()->flash('status','success');
                $request->session()->flash('message','تم حذف الإعلان بنجاح');             

            } catch (Exception $e) {

                $request->session()->flash('status','danger');
                $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
            }    

        }else{

             $request->session()->flash('status','danger');
             $request->session()->flash('message','هذا الإعلان غير موجود');

        }
       
       return redirect('/dashboard/ads');
    
    }

     public function deleteImage(Request $request,$id)
    {
       
       try {
            
            $selectedImg = AdsImgs::find($id);
            $adsId = $selectedImg->ads->id;

            $selectedImg->delete();

            $request->session()->flash('status','success');
            $request->session()->flash('message','تم حذف الصورة بنجاح');             

            return redirect('/dashboard/ads/'.$adsId.'/edit');
            
        } catch (Exception $e) {

            $request->session()->flash('status','danger');
            $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
        
            return redirect('/dashboard/ads/');
        }    
        
    }

}
