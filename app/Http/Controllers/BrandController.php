<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\Category;
use App\Http\Requests;

class BrandController extends Controller
{
    public function index()
    {
      	$brands = Brand::all();
      	$active = 'brands';
        return view('admin.brands.index',compact('brands','active'));
    }

    public function create()
    {
      	$active = 'add_brand';
      	$categories = Category::getCateogriesSelectForm();

        return view('admin.brands.create',compact('active','categories'));
    }

    public function store(Requests\CreateBrandRequest $request)
    {
        $brand = $request->all();
      
  	    if (Brand::createBrand($brand)) {
  	        
  	        $request->session()->flash('status','success');
  	        $request->session()->flash('message','تم إضافة الماركة بنجاح');             


  	    }else{
  	        
  	        $request->session()->flash('status','danger');
  	        $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
  	    }

  	    return redirect('/dashboard/brands');

    }

     public function edit($id , Request $request)
    {
        $active = 'brands';
       	$categories = Category::getCateogriesSelectForm();

        if($brand = Brand::find($id)){
        
            return view('admin.brands.edit', compact('active', 'brand','categories'));
        
        }else{

             $request->session()->flash('status','danger');
             $request->session()->flash('message','هذه الماركة غير موجود');
             return redirect('/dashboard/brands');
        }
    }

 
    public function update(Requests\EditBrandRequest $request, $id)
    {
        $brand = $request->all();          

        if (Brand::updateBrand($brand,$id)) {
                
             $request->session()->flash('status','success');
             $request->session()->flash('message','تم تعديل البيانات بنجاح');             

        }else{
             $request->session()->flash('status','danger');
             $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
        }

        return redirect('/dashboard/brands/'.$id.'/edit');


    }


     public function destroy(Request $request, $id)
    {
        $active = 'brands';

        if($brand = Brand::find($id)){

            try {
                  
                  $brand->delete();
                  $request->session()->flash('status','success');
                  $request->session()->flash('message','تم حذف الماركة بنجاح');             

            } catch (Exception $e) {

                $request->session()->flash('status','danger');
                $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
            }    
            
            return redirect('/dashboard/brands');

        }else{

             $request->session()->flash('status','danger');
             $request->session()->flash('message','هذه الماركة غير موجود');
             return redirect('/dashboard/brands');

        }
    }

     public function deleteImage(Request $request,$id)
    {
         if($brand = Brand::find($id)){

           try {
                  
                $brand->update(['img'=>'']);
                $request->session()->flash('status','success');
                $request->session()->flash('message','تم حذف الصورة بنجاح');             

            } catch (Exception $e) {

                $request->session()->flash('status','danger');
                $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
            
            }    
            
            return redirect('/dashboard/brands/'.$id.'/edit');

      }else {
      
            $request->session()->flash('status', 'danger');
            $request->session()->flash('message', 'هذه الماركة غير موجود');
            
            return redirect('dashboard/brands');        
      
      }
    }
}
