<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use App\Http\Requests;

class CommentController extends Controller
{
    public function store(Requests\CreateCommentRequest $request)
    {
        $category = $request->all();

        $category['user_id'] = \Auth::id();
      
	    if (Category::create($category)) {
	        
	        $request->session()->flash('status','success');
	        $request->session()->flash('message','تم إضافة التعليق بنجاح');             


	    }else{
	        
	        $request->session()->flash('status','danger');
	        $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
	    }

        return redirect()->back();
    }

    public function destroy(Request $request, $id)
    {
        $active = 'categories';

        if($category = Category::find($id)){

            try {
                 
              	$category->delete();
                
                $request->session()->flash('status','success');
                $request->session()->flash('message','تم حذف القسم بنجاح');             


            } catch (Exception $e) {

                $request->session()->flash('status','danger');
                $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
            }    

        }else{

             $request->session()->flash('status','danger');
             $request->session()->flash('message','هذا القسم غير موجود');
        }
		
		return redirect()->back();
    }
}
