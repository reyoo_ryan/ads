<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Requests;

class CategoryController extends Controller
{
    public function index()
    {
      	$categories = Category::all();
      	$active = 'categories';
        return view('admin.categories.index',compact('categories','active'));
    }

    public function create()
    {
      	$active = 'add_category';
      	$categories = Category::getCateogriesSelectForm('with_main');

        return view('admin.categories.create',compact('active','categories'));
    }

    public function store(Requests\CreateCategoryRequest $request)
    {
      
        $category = $request->all();
        
  	    if (Category::createCategory($category)) {
  	        
  	        $request->session()->flash('status','success');
  	        $request->session()->flash('message','تم إضافة القسم بنجاح');             


  	    }else{
  	        
  	        $request->session()->flash('status','danger');
  	        $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
  	    }

  	    return redirect('/dashboard/categories');

    }

     public function edit($id , Request $request)
    {
        $active = 'categories';
      	$categories = Category::getCateogriesSelectForm('with_main');

        if($category = Category::find($id)){

            return view('admin.categories.edit', compact('active', 'category','categories'));

        }else{

             $request->session()->flash('status','danger');
             $request->session()->flash('message','هذا القسم غير موجود');
             return redirect('/dashboard/categories');
        }
    }

 
    public function update(Requests\EditCategoryRequest $request, $id)
    {
        $category = $request->all();          

        if (Category::updateCategory($category,$id)) {
                
             $request->session()->flash('status','success');
             $request->session()->flash('message','تم تعديل البيانات بنجاح');             

        }else{
             $request->session()->flash('status','danger');
             $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
        }

        return redirect('/dashboard/categories/'.$id.'/edit');


    }


    public function destroy(Request $request, $id)
    {
        $active = 'categories';

        if($category = Category::find($id)){

            try {

                $category->delete();
                $request->session()->flash('status','success');
                $request->session()->flash('message','تم حذف القسم بنجاح');             

            } catch (Exception $e) {

                $request->session()->flash('status','danger');
                $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
            }    

        }else{

             $request->session()->flash('status','danger');
             $request->session()->flash('message','هذا القسم غير موجود');

        }
       
       return redirect('/dashboard/categories');
    
    }

     public function deleteImage(Request $request,$id)
    {
        if($category = Category::find($id)){

           try {
                  
                $category->update(['img'=>'']);
                $request->session()->flash('status','success');
                $request->session()->flash('message','تم حذف الصورة بنجاح');             

            } catch (Exception $e) {

                $request->session()->flash('status','danger');
                $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
            
            }    
            
            return redirect('/dashboard/categories/'.$id.'/edit');

      }else {
      
            $request->session()->flash('status', 'danger');
            $request->session()->flash('message', 'هذا القسم غير موجود');
            
            return redirect('dashboard/categories');        
      
      }
    }
}
