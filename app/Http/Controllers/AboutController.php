<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\About;
use App\Http\Requests;

class AboutController extends Controller
{
    public function index()
    {
      	$about = About::all()->first();
      	$active = 'about';
        return view('admin.about.edit',compact('about','active'));
    }

    public function update(Requests\EditAboutRequest $request, $id)
    {
        $about = $request->all();          

        if (About::updateAbout($about,$id)) {
                
             $request->session()->flash('status','success');
             $request->session()->flash('message','تم تعديل البيانات بنجاح');             

        }else{
             $request->session()->flash('status','danger');
             $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
        }

        return redirect('/dashboard/about');


    }

    public function deleteImage(Request $request,$id)
    {
        if($about = About::find($id)){

           try {
                  
                $about->update(['img'=>'']);
                $request->session()->flash('status','success');
                $request->session()->flash('message','تم حذف الصورة بنجاح');             

            } catch (Exception $e) {

                $request->session()->flash('status','danger');
                $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
            
            }    
            
            return redirect('/dashboard/about');

      }else {
      
            $request->session()->flash('status', 'danger');
            $request->session()->flash('message', 'حدث خطأ ما , حاول مرة اخري.');
            
            return redirect('dashboard');        
      
      }
    }
}
