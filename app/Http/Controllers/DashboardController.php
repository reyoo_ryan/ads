<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class DashboardController extends Controller
{
 
    public function index()
    {
    	$active = 'dashboard';
        return view('admin.index',compact('active'));
    }

}
