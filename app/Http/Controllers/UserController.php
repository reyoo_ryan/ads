<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests;

class UserController extends Controller
{
 
    public function index()
    {
    	$users = User::all();
    	$active = 'users';
        return view('admin.users.index',compact('users','active'));
    }

    public function create()
    {
    	$active = 'add_user';
        return view('admin.users.create',compact('active'));
    }

    public function store(Requests\CreateUserRequest $request)
    {
        $user = $request->all();
      
	    if (User::createUser($user)) {
	        
	        $request->session()->flash('status','success');
	        $request->session()->flash('message','تم إضافة المستخدم بنجاح');             


	    }else{
	        
	        $request->session()->flash('status','danger');
	        $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
	    }

	    return redirect('/dashboard/users');

    }

     public function edit($id , Request $request)
    {
        $active = 'users';

        if($user = User::find($id)){
            return view('admin.users.edit', compact('active', 'user'));
        }else{

             $request->session()->flash('status','danger');
             $request->session()->flash('message','هذا المستخدم غير موجود');
             return redirect('/dashboard/users');
        }
    }

 
    public function update(Requests\EditUserRequest $request, $id)
    {
        $user = $request->all();          

        if (User::updateUser($user,$id)) {
                
                 $request->session()->flash('status','success');
                 $request->session()->flash('message','تم تعديل البيانات بنجاح');             


        }else{
             $request->session()->flash('status','danger');
             $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
        }

        return redirect('/dashboard/users/'.$id.'/edit');


    }


     public function destroy(Request $request, $id)
    {
        $active = 'users';

        if($user = User::find($id)){

            if ($id == \Auth::id()) {

                $request->session()->flash('status','danger');
                $request->session()->flash('message','لا يمكن لمستخدم حذف نفسه.'); 
            
            }else{

                try {
                        
                        $user->delete();

                        $request->session()->flash('status','success');
                        $request->session()->flash('message','تم حذف المستخدم بنجاح');             


                    } catch (Exception $e) {

                        $request->session()->flash('status','danger');
                        $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');            
                    }    

            }

        }else{

             $request->session()->flash('status','danger');
             $request->session()->flash('message','هذا المستخدم غير موجود');

        }
        
        return redirect('/dashboard/users');
    
    }


    public function deleteImage(Request $request,$id)
    {
        if($user = User::find($id)){

            try {
                  
                $user->update(['img'=>'']);
                $request->session()->flash('status','success');
                $request->session()->flash('message','تم حذف الصورة بنجاح');             


            } catch (Exception $e) {

                $request->session()->flash('status','danger');
                $request->session()->flash('message','حدث خطأ ما , حاول مرة اخري.');
            
            }
            
            return redirect('/dashboard/users/'.$id.'/edit');

        }else {
      
            $request->session()->flash('status', 'danger');
            $request->session()->flash('message', 'هذا المستخدم غير موجود');
            
            return redirect('dashboard/users');        
      
        }
    }

    public function getUserDetials(Request $request)
    {
    	if ($request->has('user_id')) {

    		$user = User::find($request->get('user_id'));
	    	return view('admin.users.userDetails',compact('user'))->render();

    	}else{
	    	return view('admin.users.userDetails')->render();
    	}

    }


}
