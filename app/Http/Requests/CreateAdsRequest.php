<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateAdsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'   => 'required',
            'user_id' => 'required',
            'content' => 'required',
            'video'   => 'required|url',
            'brands'  => 'required',
            'price'   => 'required',
            'is_fav'  => 'required',
            'img'     => 'image',
        ];
    }
}
