<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditSettingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'siteName'      => 'required',
            'contact_email' => 'required|email',
            'keywords'      => 'required',
            'desc'          => 'required',
            'logo'          => 'image|mimes:png,jpg,jpeg',
            'favicon'       => 'mimes:ico,png,jpg,jpeg',
            'copyRight'     => 'required',
            'commission'    => 'required'
        ];
    }
}
