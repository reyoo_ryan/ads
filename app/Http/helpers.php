<?php 

	function rr() {
	    echo '<pre>';
	    array_map(function($x) {
	        print_r($x);
	    }, func_get_args());

	    echo '</pre>';
	}


    function curl_get_file_contents($URL) { //send request for api urls and recieve response 
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        $err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
        curl_close($c);
        if ($contents) return $contents;
        else return FALSE;
    }

    function now($format = 'Y-m-d H:i:s'){
    	return Carbon\Carbon::now()->format($format);
    }


    function get_last_query() { // get query sql excuted for debugging  
    	// you must use this line first 
        // \DB::connection()->enableQueryLog();
        // i used it in appserviceprovider

      $queries = DB::getQueryLog();
      $sql = end($queries);
        
      if( ! empty($sql['bindings']))
      {
        $pdo = DB::getPdo();
        foreach($sql['bindings'] as $binding)
        {
          $sql['query'] =
            preg_replace('/\?/', $pdo->quote($binding),
              $sql['query'], 1);
        }
      }
        
      return $sql['query'];
    }

 ?>