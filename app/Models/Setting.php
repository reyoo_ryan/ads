<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Request;
use Image;


class Setting extends Model
{
    protected $table = 'settings';
    protected $guarded = ['id'];
    
    public static function uploadImage($img,$typeName){

        $filename = $typeName.str_random(12).date('Y-m-d').'.'.strtolower($img->getClientOriginalExtension());
        $path = public_path('uploaded/settings/');

        if (typeName == 'logo') {
	        Image::make($img)->save($path.$filename);
        }else{
	       	$img->move($path,$filename);
        }
        
        return $filename;
    }

    public static function getAll() {

		$info = new \stdClass;

		Setting::all()->each(function ($item) use (&$info) {
				$name = $item->key;
				$value = $item->value;
				$info->$name = $value;
			});

		return $info;
	}

    public static function updateSetting($setting)
    {
        if (Request::hasFile('logo')){
           $setting['logo'] = Setting::uploadImage($setting['logo'],'logo');
        }else{
            $setting = array_except($setting, ['logo']);
        }

        if (Request::hasFile('favicon')){
           $setting['favicon'] = Setting::uploadImage($setting['favicon'],'favicon');
        }else{
            $setting = array_except($setting, ['favicon']);
        }

        try {

        	foreach ($setting as $key => $value) {

	            $updateData = Setting::where('key','=',$key)->update(['value' => $value]);

	            if(!$updateData) break;

   			 }


            return $updateData;

        } catch (Exception $e) {
            return false;
        }
    }

}
