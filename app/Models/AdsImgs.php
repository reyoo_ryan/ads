<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdsImgs extends Model
{
    protected $table = 'ads_imgs';
    protected $guarded = ['id'];

    public function ads()
    {
        return $this->belongsTo('App\Models\Ads','ad_id');
    }
    
}
