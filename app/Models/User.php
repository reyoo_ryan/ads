<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Request;
use Image;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','img','role','commission',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function ads()
    {
        return $this->hasMany('App\Models\Ads');
    }


    public function user_favs()
    {
        return $this->belongsToMany('App\Models\Ads','user_favs','user_id','ad_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }


    public static function uploadImage($img){

        $filename = 'user'.str_random(12).date('Y-m-d').'.'.strtolower($img->getClientOriginalExtension());
        $path = public_path('uploaded/users/');
        $img = Image::make($img)->save($path.$filename);
        
        return $filename;
    }

    public static function getUsersSelectForm()
    {
        $users = [];        
        foreach (User::all() as $user) {
                $users[$user->id] = $user->name . ' -> '.$user->email ;
        }
        
        return $users;
    }



    public static function createUser($user)
    {
        if (Request::hasFile('img')){
           $user['img'] = User::uploadImage($user['img']);
        }

        $user['password'] = bcrypt($user['password']);

        try {        
            return User::create($user);
        } catch (Exception $e) {
            return false;
        }

    }

    public static function updateUser($user,$id)
    {
        if (Request::hasFile('img')){
           $user['img'] = User::uploadImage($user['img']);
        }else{
            $user = array_except($user, ['img']);
        }
       
        if (!$user['password']) {
            $user = array_except($user, ['password', ]);
        }else{
            $user['password'] = bcrypt($user['password']);
        }


        try {        
            return User::find($id)->update($user);
        } catch (Exception $e) {
            return false;
        }
    }


}
