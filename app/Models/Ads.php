<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Request;
use Image;

class Ads extends Model
{
    protected $table = 'ads';
    protected $guarded = ['id'];

    public function user()
	{
	    return $this->belongsTo('App\Models\User');
	}

	public function brands()
    {
        return $this->belongsToMany('App\Models\Brand','ad_brand','ad_id','brand_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment','ad_id');
    }

    public function imgs()
    {
        return $this->hasMany('App\Models\AdsImgs','ad_id');
    }


    public static function uploadImage($img){

        $filename = 'advs_'.str_random(12).date('Y-m-d').'.'.strtolower($img->getClientOriginalExtension());
        $path = public_path('uploaded/advs/');
        $img = Image::make($img)->save($path.$filename);
        
        return $filename;
    }

    public static function createAds($ads)
    {
        if (Request::hasFile('img')){
           $ads['img'] = Ads::uploadImage($ads['img']);
        }

        try {        
            return Ads::create($ads);
        } catch (Exception $e) {
            return false;
        }

    }

    public static function updateAds($ads,$id)
    {
        if (Request::hasFile('img')){
           $ads['img'] = Ads::uploadImage($ads['img']);
        }else{
            $ads = array_except($ads, ['img']);
        }

        try {        
            return Ads::find($id)->update($ads);
        } catch (Exception $e) {
            return false;
        }
    }

}
