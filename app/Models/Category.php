<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Request;
use Image;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = ['id'];


    public function brands()
    {
        return $this->hasMany('App\Models\Brand','cat_id');
    }



    public static function uploadImage($img){

        $filename = 'category'.str_random(12).date('Y-m-d').'.'.strtolower($img->getClientOriginalExtension());
        $path = public_path('uploaded/categories/');
        $img = Image::make($img)->save($path.$filename);
        
        return $filename;
    }

    public static function getCateogriesSelectForm($with_main = null)
    {
        $with_main == null ? $categories = [] : $categories = ['0' => 'رئيسي'];
    	
    	foreach (Category::all() as $category) {
    			$categories[$category->id] = $category->title;
    	}
    	
    	return $categories;
    }

    public static function createCategory($category)
    {
        if (Request::hasFile('img')){
           $category['img'] = Category::uploadImage($category['img']);
        }

        try {        
            return Category::create($category);
        } catch (Exception $e) {
            return false;
        }

    }

    public static function updateCategory($category,$id)
    {
        if (Request::hasFile('img')){
           $category['img'] = Category::uploadImage($category['img']);
        }else{
            $category = array_except($category, ['img']);
        }

        try {        
            return Category::find($id)->update($category);
        } catch (Exception $e) {
            return false;
        }
    }
    
}
