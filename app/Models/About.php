<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Request;
use Image;

class About extends Model
{
    protected $table = 'about';
    protected $guarded = ['id'];


    public static function uploadImage($img){

        $filename = 'about'.str_random(12).date('Y-m-d').'.'.strtolower($img->getClientOriginalExtension());
        $path = public_path('uploaded/about/');
        $img = Image::make($img)->save($path.$filename);
        
        return $filename;
    }

    public static function updateAbout($about,$id)
    {
        if (Request::hasFile('img')){
           $about['img'] = About::uploadImage($about['img']);
        }else{
            $about = array_except($about, ['img']);
        }

        try {        
            return About::find($id)->update($about);
        } catch (Exception $e) {
            return false;
        }
    }
}
