<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Request;
use Image;

class Brand extends Model
{
    protected $table = 'brands';
    protected $guarded = ['id'];

    public function ads()
    {
        return $this->belongsToMany('App\Models\Ads','ad_brand','ad_id','brand_id');
    }

     public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public static function uploadImage($img){

        $filename = 'brand'.str_random(12).date('Y-m-d').'.'.strtolower($img->getClientOriginalExtension());
        $path = public_path('uploaded/brands/');
        $img = Image::make($img)->save($path.$filename);
        
        return $filename;
    }

    public static function getBrandsSelectForm($with_main = null)
    {
        $brands = [];
        
        foreach (Brand::all() as $brand) {
                $brands[$brand->id] = $brand->title;
        }
        
        return $brands;
    }


    public static function createBrand($brand)
    {
        if (Request::hasFile('img')){
           $brand['img'] = Brand::uploadImage($brand['img']);
        }

        try {        
            return Brand::create($brand);
        } catch (Exception $e) {
            return false;
        }

    }

    public static function updateBrand($brand,$id)
    {
        if (Request::hasFile('img')){
           $brand['img'] = Brand::uploadImage($brand['img']);
        }else{
            $brand = array_except($brand, ['img']);
        }

        try {        
            return Brand::find($id)->update($brand);
        } catch (Exception $e) {
            return false;
        }
    }
    
}
