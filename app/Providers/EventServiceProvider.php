<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Models\Ads;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        Ads::saved(function($ads){
        $files = \Request::file('imgs');
        $id = $ads->id;
        if(is_null($files[0])){
         return;
        }

        $data = [];
        $folder = public_path('uploaded/advs/');
        foreach ($files as $file) {
            $extension = strtolower($file->getClientOriginalExtension());
            $fullName = 'advs_' . str_random(5) . time() .'.'. $extension;
            \Image::make($file)->save($folder . $fullName);
            $data[] = ['ad_id'=>$id,'imgs'=>$fullName];
        }

        \DB::table('ads_imgs')->insert($data);

        });
    }
}
