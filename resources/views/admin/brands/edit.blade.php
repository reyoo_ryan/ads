@extends('admin.app')
@section('title', 'معاينة ماركة')
@section('page_header')

<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">معاينة ماركة</span> - لوحة التحكم</h4>
			</div>

			<!-- <div class="heading-elements">
				<div class="heading-btn-group">
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
				</div>
			</div> -->
		</div>

		<div class="breadcrumb-line">
			<ul class="breadcrumb">

				<li><a href="{{ url('/dashboard') }}"><i class="icon-home2 position-left"></i>الرئيسية</a></li>
				<li><a href="{{ url('/dashboard/brands') }}">الماركات</a></li>
				<li class="active">معاينة ماركة</li>

			</ul>

			<ul class="breadcrumb-elements">
				<!-- <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li> -->
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>
						إعدادات
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ url('/dashboard/brands') }}"><i class="icon-stack3"></i>الماركات</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /page header -->

@stop

@section('content')

		<!-- Basic setup -->
        <div class="panel panel-white">
			<div class="panel-heading">
				<h6 class="panel-title">معاينة قسم</h6>
				<!-- <div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="collapse"></a></li>
                		<li><a data-action="reload"></a></li>
                		<li><a data-action="close"></a></li>
                	</ul>
            	</div> -->
			</div>

        	 {!! Form::model($brand,array(
              'url' => [url('/dashboard/brands',$brand->id)],
              'method' => 'PUT',
              'class' => 'form-basic',
              'files' => true,

      		)) !!}
				<fieldset class="step" id="step1">
					<!-- <h6 class="form-wizard-title text-semibold">
						<span class="form-wizard-count">1</span>
						Personal info
						<small class="display-block">Tell us a bit about yourself</small>
					</h6> -->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="title">عنوان الماركة : </label>
								
								{!! Form::text('title',null,['class'=>'form-control','id' => 'title']) !!}
								{!! $errors->first('title' , '<p class="validation-error-label">:message</span></p>') !!}

							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="cat_id">القسم : </label>
								
								{!!  Form::select('cat_id', $categories, $brand->cat_id, ['class' => 'bootstrap-select','id' => 'cat_id','data-width' => '100%']); !!}
								{!! $errors->first('title' , '<p class="validation-error-label">:message</span></p>') !!}

							</div>
						</div>

					</div>

					<div class="row">

						<div class="col-md-6">
							<div class="form-group">
								<label for="desc">الوصف :  </label>
								{!! Form::textarea('desc',null,['class'=>'form-control','rows' => '5', 'cols' => '5','id' => 'desc']) !!}
								{!! $errors->first('desc' , '<p class="validation-error-label">:message</span></p>') !!}
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="img" class="text-semibold">الصورة : </label>
								<div class="media no-margin-top">
									<div class="media-left">
										@if (!$brand->img)

											<img src="/admin/assets/images/placeholder.jpg" style="width: 58px; height: 58px; border-radius: 2px;">
										@else
											<a data-toggle="modal" data-target="#delete_brand_image" data-popup="tooltip" title="" data-placement="bottom" data-original-title="حذف">
												<img src="/images/58x58/{{ $brand->img }}" style="width: 58px; height: 58px; border-radius: 2px;">
											</a>

										@endif
									</div>

									<div class="media-body">
										<input type="file" class="file-styled" id="img" name="img">
										<span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</fieldset>

				<div class="form-wizard-actions">
					<button class="btn btn-info" type="submit">حفظ</button>
				</div>
			{!! Form::close() !!}

			@if ($brand->img)
				<div id="delete_brand_image" class="modal fade in">
					{!! Form::open(array(
					'url' => [url('/dashboard/brands/deleteImage',$brand->id)],
					'method' => 'DELETE',
					'class' => 'delete',
					)) !!}
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">×</button>
									<h6 class="modal-title">تأكيد الحذف</h6>
								</div>

								<div class="modal-body">
									<p>تأكيد حذف صورة المستخدم ؟</p>
									<hr>
								</div>

								<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">إلغاء</button>
										<button type="submit" class="btn btn-primary">حذف</button>
								</div>
							</div>
						</div>
					{!! Form::close() !!}	
				</div>
			@endif

        </div>
        <!-- /basic setup -->

@stop