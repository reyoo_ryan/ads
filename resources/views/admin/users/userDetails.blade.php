<div class="row">

	<div class="col-md-6">
		<div class="form-group">
			<label for="phone">الهاتف :  </label>
			{!! Form::text('phone',old('phone') != [] ? old('phone') : isset($user) ? $user->phone : null ,['class'=>'form-control','id' => 'phone']) !!}
		    {!! $errors->first('phone' , '<p class="validation-error-label">:message</span></p>') !!}

		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
			<label for="commission">العمولة المخصومة :  </label>
			{!! Form::text('commission',old('commission') != [] ? old('commission') : isset($user) ? $user->commission : null ,['class'=>'form-control','id' => 'commission']) !!}
			<span class="help-block">يمكن تركها فارغة و تطبيق الاعداد العام.</span>			
		    {!! $errors->first('commission' , '<p class="validation-error-label">:message</span></p>') !!}

		</div>
	</div>

</div>