@extends('admin.app')
@section('title', 'معاينة مستخدم')
@section('page_header')

<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">معاينة مستخدم</span> - لوحة التحكم</h4>
			</div>

			<!-- <div class="heading-elements">
				<div class="heading-btn-group">
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
				</div>
			</div> -->
		</div>

		<div class="breadcrumb-line">
			<ul class="breadcrumb">

				<li><a href="{{ url('/dashboard') }}"><i class="icon-home2 position-left"></i>الرئيسية</a></li>
				<li><a href="{{ url('/dashboard/users') }}">المستخدمين</a></li>
				<li class="active">معاينة مستخدم</li>

			</ul>

			<ul class="breadcrumb-elements">
				<!-- <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li> -->
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>
						إعدادات
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ url('/dashboard/users') }}"><i class="icon-users"></i>المستخدمين</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /page header -->

@stop

@section('content')

		<!-- Basic setup -->
        <div class="panel panel-white">
			<div class="panel-heading">
				<h6 class="panel-title">معاينة مستخدم</h6>
				<!-- <div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="collapse"></a></li>
                		<li><a data-action="reload"></a></li>
                		<li><a data-action="close"></a></li>
                	</ul>
            	</div> -->
			</div>

        	 {!! Form::model($user,array(
              'url' => [url('/dashboard/users',$user->id)],
              'method' => 'PUT',
              'class' => 'form-basic users',
              'files' => true,

      		)) !!}
				<fieldset class="step" id="step1">
					<!-- <h6 class="form-wizard-title text-semibold">
						<span class="form-wizard-count">1</span>
						Personal info
						<small class="display-block">Tell us a bit about yourself</small>
					</h6> -->
					<input type="hidden" class="edit-user" value="{{ $user->id }}">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="role">نوع المستخدم : </label>
								{!!  Form::select('role', array('admin' => 'مدير', 'member' => 'عضو'), null, ['class' => 'bootstrap-select','id' => 'role','data-width' => '100%']); !!}

								{!! $errors->first('role' , '<p class="validation-error-label">:message</span></p>') !!}

							</div>
						</div>

					</div>

					<div class="row">

						<div class="col-md-6">
							<div class="form-group">
								<label for="name">الاسم :  </label>
								{!! Form::text('name',null,['class'=>'form-control','id' => 'name']) !!}
							    {!! $errors->first('name' , '<p class="validation-error-label">:message</span></p>') !!}

							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="email">البريد الالكتروني : </label>
								{!! Form::text('email',null,['class'=>'form-control','id' => 'email']) !!}
 						        {!! $errors->first('email' , '<p class="validation-error-label">:message</span></p>') !!}
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="password">كلمة المرور : </label>
								{!! Form::password('password',['class'=>'form-control','id' => 'password']) !!}
 						        {!! $errors->first('password' , '<p class="validation-error-label">:message</span></p>') !!}

							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="password_confirmation">تأكيد كلمة المرور : </label>
								{!! Form::password('password_confirmation',['class'=>'form-control','id' => 'password_confirmation']) !!}
 						        {!! $errors->first('password_confirmation' , '<p class="validation-error-label">:message</span></p>') !!}

							</div>
						</div>
					</div>

					<div id="userDetails">
						@if ( $user->role == 'member' && old('role') != 'admin')
							@include('admin.users.userDetails')
						@endif
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="img" class="text-semibold">الصورة : </label>
								<div class="media no-margin-top">
									<div class="media-left">
										@if (!$user->img)

											<img src="/admin/assets/images/placeholder.jpg" style="width: 58px; height: 58px; border-radius: 2px;">
										@else
											<a data-toggle="modal" data-target="#delete_user_image" data-popup="tooltip" title="" data-placement="bottom" data-original-title="حذف">
												<img src="/images/58x58/{{ $user->img }}" style="width: 58px; height: 58px; border-radius: 2px;">
											</a>

										@endif
									</div>

									<div class="media-body">
										<input type="file" class="file-styled" id="img" name="img">
										<span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</fieldset>

				<div class="form-wizard-actions">
					<button class="btn btn-info" type="submit">حفظ</button>
				</div>
			{!! Form::close() !!}

			@if ($user->img)
				<div id="delete_user_image" class="modal fade in">
					{!! Form::open(array(
					'url' => [url('/dashboard/users/deleteImage',$user->id)],
					'method' => 'DELETE',
					'class' => 'delete',
					)) !!}
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">×</button>
									<h6 class="modal-title">تأكيد الحذف</h6>
								</div>

								<div class="modal-body">
									<p>تأكيد حذف صورة المستخدم ؟</p>
									<hr>
								</div>

								<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">إلغاء</button>
										<button type="submit" class="btn btn-primary">حذف</button>
								</div>
							</div>
						</div>
					{!! Form::close() !!}	
				</div>
			@endif

        </div>
        <!-- /basic setup -->

@stop