@extends('admin.app')
@section('title', 'من نحن')
@section('page_header')

<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">من نحن</span> - لوحة التحكم</h4>
			</div>

			<!-- <div class="heading-elements">
				<div class="heading-btn-group">
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
				</div>
			</div> -->
		</div>

		<div class="breadcrumb-line">
			<ul class="breadcrumb">

				<li><a href="{{ url('/dashboard') }}"><i class="icon-home2 position-left"></i>الرئيسية</a></li>
				<li class="active">من نحن</li>

			</ul>

		</div>
	</div>
	<!-- /page header -->

@stop

@section('content')

		<!-- Basic setup -->
        <div class="panel panel-white">
			<div class="panel-heading">
				<h6 class="panel-title">من نحن</h6>
				<!-- <div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="collapse"></a></li>
                		<li><a data-action="reload"></a></li>
                		<li><a data-action="close"></a></li>
                	</ul>
            	</div> -->
			</div>

        	 {!! Form::model($about,array(
              'url' => [url('/dashboard/about',$about->id)],
              'method' => 'PUT',
              'class' => 'form-basic',
              'files' => true,

      		)) !!}
				<fieldset class="step" id="step1">
					<!-- <h6 class="form-wizard-title text-semibold">
						<span class="form-wizard-count">1</span>
						Personal info
						<small class="display-block">Tell us a bit about yourself</small>
					</h6> -->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="title">العنوان : </label>
								
								{!! Form::text('title',null,['class'=>'form-control','id' => 'title']) !!}
								{!! $errors->first('title' , '<p class="validation-error-label">:message</span></p>') !!}

							</div>
						</div>

					</div>

					<div class="row">

						<div class="col-md-6">
							<div class="form-group">
								<label for="content">المحتوي :  </label>
								{!! Form::textarea('content',null,['class'=>'form-control','rows' => '5', 'cols' => '5','id' => 'content']) !!}
								{!! $errors->first('content' , '<p class="validation-error-label">:message</span></p>') !!}
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="img" class="text-semibold">الصورة : </label>
								<div class="media no-margin-top">
									<div class="media-left">
										@if (!$about->img)

											<img src="/admin/assets/images/placeholder.jpg" style="width: 58px; height: 58px; border-radius: 2px;">
										@else
											<a data-toggle="modal" data-target="#delete_about_image" data-popup="tooltip" title="" data-placement="bottom" data-original-title="حذف">
												<img src="/images/58x58/{{ $about->img }}" style="width: 58px; height: 58px; border-radius: 2px;">
											</a>

										@endif
									</div>

									<div class="media-body">
										<input type="file" class="file-styled" id="img" name="img">
										<span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</fieldset>

				<div class="form-wizard-actions">
					<button class="btn btn-info" type="submit">حفظ</button>
				</div>
			{!! Form::close() !!}

			@if ($about->img)
				<div id="delete_about_image" class="modal fade in">
					{!! Form::open(array(
					'url' => [url('/dashboard/about/deleteImage',$about->id)],
					'method' => 'DELETE',
					'class' => 'delete',
					)) !!}
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">×</button>
									<h6 class="modal-title">تأكيد الحذف</h6>
								</div>

								<div class="modal-body">
									<p>تأكيد حذف صورة المستخدم ؟</p>
									<hr>
								</div>

								<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">إلغاء</button>
										<button type="submit" class="btn btn-primary">حذف</button>
								</div>
							</div>
						</div>
					{!! Form::close() !!}	
				</div>
			@endif

        </div>
        <!-- /basic setup -->

@stop