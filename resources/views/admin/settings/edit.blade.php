@extends('admin.app')
@section('title', 'إعدادات الموقع')
@section('page_header')

<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">اعدادات الموقع</span> - لوحة التحكم</h4>
			</div>

			<!-- <div class="heading-elements">
				<div class="heading-btn-group">
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
				</div>
			</div> -->
		</div>

		<div class="breadcrumb-line">
			<ul class="breadcrumb">

				<li><a href="{{ url('/dashboard') }}"><i class="icon-home2 position-left"></i>الرئيسية</a></li>
				<li class="active">اعدادات الموقع</li>

			</ul>

		</div>
	</div>
	<!-- /page header -->

@stop

@section('content')

		<!-- Basic setup -->
        <div class="panel panel-white">
			<div class="panel-heading">
				<h6 class="panel-title">اعدادات الموقع</h6>
				<!-- <div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="collapse"></a></li>
                		<li><a data-action="reload"></a></li>
                		<li><a data-action="close"></a></li>
                	</ul>
            	</div> -->
			</div>

        	 {!! Form::model($setting,array(
              'url' => [url('/dashboard/settings/0')],
              'method' => 'PUT',
              'class' => 'form-basic',
              'files' => true,

      		)) !!}
				<fieldset class="step" id="step1">
					<!-- <h6 class="form-wizard-title text-semibold">
						<span class="form-wizard-count">1</span>
						Personal info
						<small class="display-block">Tell us a bit about yourself</small>
					</h6> -->
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="siteName">عنوان الموقع : </label>
								
								{!! Form::text('siteName',null,['class'=>'form-control','id' => 'siteName']) !!}
								{!! $errors->first('siteName' , '<p class="validation-error-label">:message</span></p>') !!}

							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="contact_email">بريد التواصل : </label>
								
								{!! Form::text('contact_email',null,['class'=>'form-control','id' => 'contact_email']) !!}
								{!! $errors->first('contact_email' , '<p class="validation-error-label">:message</span></p>') !!}

							</div>
						</div>

					</div>

					<div class="row">

						<div class="col-md-6">
							<div class="form-group">
								<label for="keywords">الكلمات الدلاليه : </label>
								
								{!! Form::text('keywords',null,['class'=>'form-control','id' => 'keywords']) !!}
								<span class="help-block">افصل بين كل كلمة واخري بعلامة الفاصلة ( , )</span>
								{!! $errors->first('keywords' , '<p class="validation-error-label">:message</span></p>') !!}

							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="desc">وصف الموقع :  </label>

								{!! Form::textarea('desc',null,['class'=>'form-control','rows' => '5', 'cols' => '5','id' => 'desc']) !!}
								{!! $errors->first('desc' , '<p class="validation-error-label">:message</span></p>') !!}
							</div>
						</div>
					</div>

					<div class="row">

						<div class="col-md-6">
							<div class="form-group">
								<label for="commission">العمولة المخصومة : </label>
								
								{!! Form::text('commission',null,['class'=>'form-control','id' => 'commission']) !!}
								{!! $errors->first('commission' , '<p class="validation-error-label">:message</span></p>') !!}

							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="copyRight">حقوق المكلية :  </label>

								{!! Form::textarea('copyRight',null,['class'=>'form-control','rows' => '5', 'cols' => '5','id' => 'copyRight']) !!}
								{!! $errors->first('copyRight' , '<p class="validation-error-label">:message</span></p>') !!}
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="favicon" class="text-semibold">ايقونة الموقع : </label>
								<div class="media no-margin-top">
									<div class="media-left">
										@if (!$setting->favicon)

											<img src="/admin/assets/images/placeholder.jpg" style="width: 58px; height: 58px; border-radius: 2px;">
										@else
												<img src="/uploaded/settings/{{ $setting->favicon }}" style="width: 58px; height: 58px; border-radius: 2px;">

										@endif
									</div>
									<div class="media-body">
										<input type="file" class="file-styled" id="favicon" name="favicon">
										<span class="help-block">Accepted formats: ico,png,jpg,jpeg. Max file size 2Mb</span>
										
										{!! $errors->first('favicon' , '<p class="validation-error-label">:message</span></p>') !!}
									</div>
								</div>
							</div>
						</div>



						<div class="col-md-6">
							<div class="form-group">
								<label for="logo" class="text-semibold">صورة الشعار : </label>
								<div class="media no-margin-top">
									<div class="media-left">
										@if (!$setting->logo)

											<img src="/admin/assets/images/placeholder.jpg" style="width: 58px; height: 58px; border-radius: 2px;">
										@else
											<img src="/images/58x58/{{ $setting->logo }}" style="width: 58px; height: 58px; border-radius: 2px;">
										@endif
									</div>

									<div class="media-body">
										<input type="file" class="file-styled" id="logo" name="logo">
										<span class="help-block">Accepted formats: png,jpg,jpeg. Max file size 2Mb</span>
										{!! $errors->first('logo' , '<p class="validation-error-label">:message</span></p>') !!}
									</div>
								</div>
							</div>
						</div>


					</div>
				</fieldset>

				<div class="form-wizard-actions">
					<button class="btn btn-info" type="submit">حفظ</button>
				</div>
			{!! Form::close() !!}

        </div>
        <!-- /basic setup -->

@stop