	<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="{{ Auth::user()->img == '' ? '/admin/assets/images/placeholder.jpg' : '/images/58x58/'.Auth::user()->img }}" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">{{ Auth::user()->name }}</span>
									<div class="text-size-mini text-muted">
										<i class="text-size-small"></i>{{ Auth::user()->email }}
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>الرئيسية</span> <i class="icon-menu" title="Main pages"></i></li>

								<li class="{{ $active == 'dashboard' ? 'active' : ''}}"><a href="{{ url('/dashboard') }}"><i class="icon-home4"></i> <span>لوحة التحكم</span></a></li>

								<li class="{{ $active == 'settings' ? 'active' : ''}}"><a href="{{ url('/dashboard/settings') }}"><i class="icon-cog4"></i> <span>إعدادات الموقع</span></a></li>

								<li class="{{ $active == 'about' ? 'active' : ''}}"><a href="{{ url('/dashboard/about') }}"><i class="icon-info22"></i> <span>من نحن</span></a></li>

								<li>
									<a><i class="icon-users"></i> <span>المستخدمين</span></a>
									<ul>
										<li class="{{ $active == 'users' ? 'active' : ''}}"><a href="{{ url('/dashboard/users') }}">عرض الكل</a></li>
										<li class="{{ $active == 'add_user' ? 'active' : ''}}"><a href="{{ url('/dashboard/users/create') }}">إضافة مستخدم</a></li>
									</ul>
								</li>

								<li>
									<a><i class="icon-stack3"></i> <span>الأقسام</span></a>
									<ul>
										<li class="{{ $active == 'categories' ? 'active' : ''}}"><a href="{{ url('/dashboard/categories') }}">عرض الكل</a></li>
										<li class="{{ $active == 'add_category' ? 'active' : ''}}"><a href="{{ url('/dashboard/categories/create') }}">إضافة قسم</a></li>
									</ul>
								</li>

								<li>
									<a><i class="icon-stars"></i> <span>الماركات</span></a>
									<ul>
										<li class="{{ $active == 'brands' ? 'active' : ''}}"><a href="{{ url('/dashboard/brands') }}">عرض الكل</a></li>
										<li class="{{ $active == 'add_brand' ? 'active' : ''}}"><a href="{{ url('/dashboard/brands/create') }}">إضافة ماركة</a></li>
									</ul>
								</li>

								<li>
									<a><i class="icon-newspaper"></i> <span>الإعلانات</span></a>
									<ul>
										<li class="{{ $active == 'ads' ? 'active' : ''}}"><a href="{{ url('/dashboard/ads') }}">عرض الكل</a></li>
										<li class="{{ $active == 'add_ads' ? 'active' : ''}}"><a href="{{ url('/dashboard/ads/create') }}">إضافة إعلان</a></li>
									</ul>
								</li>

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->