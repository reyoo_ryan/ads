@extends('admin.app')
@section('title', 'الإعلانات')
@section('page_header')

<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">الإعلانات</span> - لوحة التحكم</h4>
			</div>

			<!-- <div class="heading-elements">
				<div class="heading-btn-group">
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
				</div>
			</div> -->
		</div>

		<div class="breadcrumb-line">
			<ul class="breadcrumb">

				<li><a href="{{ url('/dashboard') }}"><i class="icon-home2 position-left"></i>الرئيسية</a></li>
				<li class="active">الإعلانات</li>

			</ul>

			<ul class="breadcrumb-elements">
				<!-- <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li> -->
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>
						إعدادات
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ url('/dashboard/ads/create') }}"><i class="icon-plus"></i> إضافة إعلان</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /page header -->

@stop

@section('content')

		<!-- Basic datatable -->
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">الإعلانات</h5>
				<!-- <div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="collapse"></a></li>
                		<li><a data-action="reload"></a></li>
                		<li><a data-action="close"></a></li>
                	</ul>
            	</div> -->
			</div>

			<!-- <div class="panel-body">
				The <code>DataTables</code> is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table. DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function. Searching, ordering, paging etc goodness will be immediately added to the table, as shown in this example. <strong>Datatables support all available table styling.</strong>
			</div>
 -->
			<table class="table datatable-basic">
				<thead>
					<tr>
						<th>عنوان الإعلان</th>
						<th>السعر</th>
						<th>مفضل</th>
						<th class="text-center">العمليات</th>
					</tr>
				</thead>
				<tbody>

					@foreach ($ads as $ad)
						<tr>
							<td>{{ $ad->title }}</td>
							<td>{{ $ad->price }}</td>
							<td>{{ $ad->is_fav == 1 ? 'نعم' : 'لا'  }}</td>
							<td class="text-center">
								<ul class="icons-list">
									<li class="text-info-600"><a href="{{ url('dashboard/ads/'.$ad->id.'/edit') }}" data-popup="tooltip" title="معاينة"><i class="icon-file-eye2"></i></a></li>
									{{--  <li class="text-info-600"><a href="{{ url('dashboard/ads/$ad->id') }}" data-popup="tooltip" title="عرض"><i class="icon-info22"></i></a></li> --}}
									<li class="text-danger-600"><a data-toggle="modal" data-target="#delete_ad_{{ $ad->id }}" data-popup="tooltip" title="حذف"><i class="icon-trash"></i></a></li>
								</ul>

							</td>
						</tr>
					@endforeach

				</tbody>
			</table>

			@foreach ($ads as $ad)
				<div id="delete_ad_{{ $ad->id }}" class="modal fade in">
					{!! Form::open(array(
					'url' => [url('/dashboard/ads',$ad->id)],
					'method' => 'DELETE',
					'class' => 'delete',
					)) !!}
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">×</button>
									<h6 class="modal-title">تأكيد الحذف</h6>
								</div>

								<div class="modal-body">
									<p>تأكيد حذف اعلان : {{ $ad->title }}</p>
									<hr>
								</div>

								<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">إلغاء</button>
										<button type="submit" class="btn btn-primary">حذف</button>
								</div>
							</div>
						</div>
					{!! Form::close() !!}	
				</div>
			@endforeach
		</div>
		<!-- /basic datatable -->

@stop