@extends('admin.app')
@section('title', 'إضافة إعلان')
@section('page_header')

<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">إضافة إعلان</span> - لوحة التحكم</h4>
			</div>

			<!-- <div class="heading-elements">
				<div class="heading-btn-group">
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
					<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
				</div>
			</div> -->
		</div>

		<div class="breadcrumb-line">
			<ul class="breadcrumb">

				<li><a href="{{ url('/dashboard') }}"><i class="icon-home2 position-left"></i>الرئيسية</a></li>
				<li><a href="{{ url('/dashboard/ads') }}">الإعلانات</a></li>
				<li class="active">إضافة إعلان</li>

			</ul>

			<ul class="breadcrumb-elements">
				<!-- <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li> -->
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-gear position-left"></i>
						إعدادات
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ url('/dashboard/ads') }}"><i class="icon-stack3"></i>الإعلانات</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /page header -->

@stop

@section('content')

		<!-- Basic setup -->
        <div class="panel panel-white">
			<div class="panel-heading">
				<h6 class="panel-title">إضافة إعلان</h6>
				<!-- <div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="collapse"></a></li>
                		<li><a data-action="reload"></a></li>
                		<li><a data-action="close"></a></li>
                	</ul>
            	</div> -->
			</div>

        	 {!! Form::open(array(
              'url' => [url('/dashboard/ads')],
              'method' => 'POST',
              'class' => 'form-basic',
              'files' => true,

      		)) !!}
				<fieldset class="step" id="step1">
					<!-- <h6 class="form-wizard-title text-semibold">
						<span class="form-wizard-count">1</span>
						Personal info
						<small class="display-block">Tell us a bit about yourself</small>
					</h6> -->

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="title">عنوان الإعلان : </label>
								
								{!! Form::text('title',null,['class'=>'form-control','id' => 'title']) !!}
								{!! $errors->first('title' , '<p class="validation-error-label">:message</span></p>') !!}

							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="user_id">صاحب الإعلان : </label>
								{!!  Form::select('user_id', $users, old('user_id') OR Auth::id(), ['class' => 'bootstrap-select','data-live-search' => 'true','id' => 'user_id','data-width' => '100%']); !!}
							</div>
						</div>

					</div>

					<div class="row">

						<div class="col-md-6">
							<div class="form-group">
								<label for="content">المحتوي :  </label>
								{!! Form::textarea('content',null,['class'=>'form-control','rows' => '5', 'cols' => '5','id' => 'content']) !!}
								{!! $errors->first('content' , '<p class="validation-error-label">:message</span></p>') !!}
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="video">رابط الفيديو : </label>
								
								{!! Form::text('video',null,['class'=>'form-control','id' => 'video']) !!}
								{!! $errors->first('video' , '<p class="validation-error-label">:message</span></p>') !!}

							</div>
						</div>

					</div>

					<div class="row">
					
						<div class="col-md-6">
							<div class="form-group">
								<label for="keywords">الكلمات الدلالية : </label>
								
								{!! Form::text('keywords',null,['class'=>'form-control','id' => 'keywords']) !!}
								<span class="help-block">افصل بين كل كلمة واخري بعلامة الفاصلة ( , )</span>
								{!! $errors->first('keywords' , '<p class="validation-error-label">:message</span></p>') !!}

							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="desc">وصف الإعلان :  </label>
								{!! Form::textarea('desc',null,['class'=>'form-control','rows' => '5', 'cols' => '5','id' => 'desc']) !!}
								{!! $errors->first('desc' , '<p class="validation-error-label">:message</span></p>') !!}
							</div>
						</div>

					</div>
					
					<div class="row">

						<div class="col-md-6">

							<div class="form-group">
								<label>الماركات : </label>
								
								<select name="brands[]" class="form-control" multiple="multiple">
									@foreach ($categories as $category)
										@if ($category->brands->count())
											<optgroup label="{{ $category->title }}">
												@foreach ($category->brands as $brand)
													<option value="{{ $brand->id }}" {{ old('brands') != [] ? in_array($brand->id,old('brands')) ? 'selected' : '' : '' }}>{{ $brand->title }}</option>
												@endforeach		
											</optgroup>
										@endif
									@endforeach
								</select>
								{!! $errors->first('brands' , '<p class="validation-error-label">:message</span></p>') !!}
							</div>

						</div>

					</div>

					<div class="row">

						<div class="col-md-6">
							<div class="form-group">
								<label for="price">السعر : </label>
								
								{!! Form::text('price',null,['class'=>'form-control','id' => 'price']) !!}
								{!! $errors->first('price' , '<p class="validation-error-label">:message</span></p>') !!}

							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label class="display-block">إعلان مفضَل : </label>

								<label class="radio-inline">
									{!!  Form::radio('is_fav', '1', false,['class' => 'styled']) !!}
									نعم
								</label>

								<label class="radio-inline">
								{!!  Form::radio('is_fav', '0', true,['class' => 'styled']) !!}
									لا
								</label>
								
								{!! $errors->first('is_fav' , '<p class="validation-error-label">:message</span></p>') !!}
							</div>
						</div>

					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="img" class="text-semibold">الصورة الرئيسية : </label>
								<div class="media no-margin-top">
									<div class="media-left">
										<a href="#"><img src="/admin/assets/images/placeholder.jpg" style="width: 58px; height: 58px; border-radius: 2px;" alt=""></a>
									</div>

									<div class="media-body">
										<input type="file" class="file-styled" id="img" name="img">
										<span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="imgs" class="text-semibold">الصور الإضافية : </label>
								<div class="media no-margin-top">
									<div class="media-body">
										<input type="file" class="file-styled" multiple="true" id="imgs" name="imgs[]">
										@if (Session::has('custErrors'))
											<p class="validation-error-label">{{ Session::get('custErrors')['imgs[]'] }}</span></p>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</fieldset>

				<div class="form-wizard-actions">
					<button class="btn btn-info" type="submit">حفظ</button>
				</div>
			{!! Form::close() !!}
        </div>
        <!-- /basic setup -->

@stop