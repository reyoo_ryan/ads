<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate();
		$keys = [  'logo','favicon','contact_email', 'siteName',
					'keywords','desc','copyRight','commission'
				];
		$values = [];

		foreach ($keys as $value) {
			$values[] = ['key' => $value, 'value' => $value.'_Test'];
		}

		DB::table('settings')->insert($values);
    }
}
